const Library = require("../models/library.models");

const getAllLibraries = async (req, res, next) => {
  try {
    const allLibraries = await Library.find().populate("books");
    res.status(200).json(allLibraries);
  } catch (error) {
    return next(error);
  }
};

const postNewLibrary = async (req, res, next) => {
  try {
    const newLibrary = new Library(req.body);
    const libraInBd = await newLibrary.save();
    return res.status(201).json(newLibrary);
  } catch (error) {
    return next(error);
  }
};

const patchNewBookInLibra = async (req, res, next) => {
  try {
    const { id } = req.params;
    const idBook = req.body.idBook;
    const updateLibraryWithBook = await Library.findByIdAndUpdate(id, {
      $push: { books: idBook },
    });
    return res.status(200).json(updateLibraryWithBook);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  getAllLibraries,
  postNewLibrary,
  patchNewBookInLibra,
};
