const upload = require("../../middelwares/file.middlelwares");
const Book = require("../models/book.models");

const getAllBooks = async (req, res, next) => {
  try {
    const allBooks = await Book.find();
    return res.status(200).json(allBooks);
  } catch (error) {
    return next(error);
  }
};

const getBookById = async (req, res, next) => {
  try {
    //const id = req.params.id
    //const { id } = req.params
    const bookById = await Book.findById(req.params.id);
    return res.status(200).json(bookById);
  } catch (error) {
    return next(error);
  }
};

const getBookByTitle = async (req, res, next) => {
  try {
    const { title } = req.params;
    const bookByTitle = await Book.find({ title });
    return res.status(200).json(bookByTitle);
  } catch (error) {
    return next(error);
  }
};

const getBookByYear = async (req, res, next) => {
  try {
    const { year } = req.params;
    const bookById = await Book.find({ year });
    return res.status(200).json(bookById);
  } catch (error) {
    return next(error);
  }
};

const postNewBook = async (req, res, next) => {
  try {
    const newBook = new Book({
      title: req.body.title,
      writer: req.body.writer,
      year: req.body.year,
    });
    newBook.image = req.file.path;
    const newBookInBd = await newBook.save();
    return res.status(201).json(newBookInBd);
  } catch (error) {
    return res.status(500).send(error);
  }
};

const deleteBookByTitle = async (req, res, next) => {
  try {
    const { title } = req.params;
    const bookdeleteByTitle = await Book.findOneAndDelete({ title });
   

    return res.status(200).json(bookdeleteByTitle);
  } catch (error) {
    return res.status(500).send(error);
  }
};

const putBookById = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { title } = req.body;
    const { writer } = req.body;
    const { year } = req.body;

    const updated = await Book.findOneAndReplace(id, { title, writer, year });

    res.json(updated);
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports = {
  getAllBooks,
  getBookById,
  getBookByTitle,
  getBookByYear,
  postNewBook,
  deleteBookByTitle,
  putBookById,
};
