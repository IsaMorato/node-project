const Socio = require("../models/Socio.models");

const getAllSocios = async (req, res, next) => {
  try {
    const allSocios = await Socio.find().populate("books");

    res.status(200).json(allSocios);
  } catch (error) {
    return next(error);
  }
};

const getSocioById = async (req, res, next) => {
  try {
    
    const socioById = await Socio.findById(req.params.id);
    return res.status(200).json(socioById);
  } catch (error) {
    return next(error);
  }
};

const postNewSocio = async (req, res, next) => {
  try {
    const newSocio = new Socio(req.body);
    const socioInBd = await newSocio.save();
    return res.status(201).json(newSocio);
  } catch (error) {
    return next(error);
  }
};
const deleteSocio = async (req, res, next) => {
  try {
    const sociodeleteById = await Socio.findByIdAndDelete(req.params.id);

    res.status(204).send(sociodeleteById);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getAllSocios,
  getSocioById,
  postNewSocio,
  deleteSocio,
};
