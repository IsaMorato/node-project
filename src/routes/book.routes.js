const bookRouter = require("express").Router();

const upload = require("../../middelwares/file.middlelwares");

const {
  getAllBooks,
  postNewBook,
  getBookById,
  getBookByTitle,
  getBookByYear,
  deleteBookByTitle,
  putBookById,
} = require("../controllers/book.controller");

bookRouter.get("/", getAllBooks);
bookRouter.post("/", upload.single("image"), postNewBook);
bookRouter.get("/:id", getBookById);
bookRouter.delete("/title/:title", deleteBookByTitle);
bookRouter.get("/title/:title", getBookByTitle);
bookRouter.get("/year/:year", getBookByYear);
bookRouter.put("/:id", putBookById);

module.exports = bookRouter;
