const libraryRouter = require("express").Router();
const {
  getAllLibraries,
  postNewLibrary,
  patchNewBookInLibra,
} = require("../controllers/library.controllers");

libraryRouter.get("/", getAllLibraries);
libraryRouter.post("/", postNewLibrary);
libraryRouter.patch("/:id", patchNewBookInLibra);

module.exports = libraryRouter;
