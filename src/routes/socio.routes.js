const socioRouter = require("express").Router();

const {
  getAllSocios,
  postNewSocio,
  getSocioById,
  deleteSocio,
} = require("../controllers/socio.controller");

socioRouter.get("/", getAllSocios);
socioRouter.post("/", postNewSocio);
socioRouter.get("/:id", getSocioById);
socioRouter.delete("/:id", deleteSocio);

module.exports = socioRouter;
