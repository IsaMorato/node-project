const express = require("express");

const { connectWithDb } = require("./utils/db/db");
const bookRoutes = require("./routes/book.routes");
const libraryRoutes = require("./routes/library.routes");
const cloudinary = require("cloudinary").v2;
const usersRoutes = require("./routes/user.routes");

const socioRoutes = require("./routes/socio.routes");


const PORT = 3000;

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
});
const app = express();

connectWithDb();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/users", usersRoutes);
app.use("/books", bookRoutes);
app.use("/libraries", libraryRoutes);
app.use("/socio", socioRoutes);


app.use("*", (req, res, next) => {
  const error = new Error();
  error.status = 404;
  error.message = "Route not found";
  return next(error);
});

app.use((error, req, res, next) => {
  return res
    .status(error.status || 500)
    .json(error.message || "Unexpected error");
});
app.disable("x-powered-by");

app.listen(PORT, () => {
  console.log("Server is running in http://localhost:" + PORT);
});
