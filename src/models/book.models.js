const mongoose = require("mongoose");

const bookSchema = new mongoose.Schema(
  {
    title: { type: String, required: true, trim: true },
    writer: { type: String, required: true, trim: true },
    year: { type: Number, required: true, trim: true },
    image: { type: String },
  },
  {
    timestamps: true,
  }
);

const Book = mongoose.model("books", bookSchema);
module.exports = Book;
