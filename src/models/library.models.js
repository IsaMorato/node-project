const mongoose = require("mongoose");

const librarySchema = new mongoose.Schema(
  {
    name: { type: String, required: true, trim: true },
    city: { type: String, required: true, trim: true },
    books: [{ type: mongoose.Types.ObjectId, ref: "books", required: false }],
  },
  {
    timestamps: true,
  }
);

const Library = mongoose.model("libraries", librarySchema);
module.exports = Library;
