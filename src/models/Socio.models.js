const mongoose = require('mongoose');



const socioSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    surname: { type: String, required: true },
    phone: { type: String, required: false },
    books: [ {type: mongoose.Types.ObjectId, ref: 'books'} ]
    
    
    
  },
  {
    timestamps: true,
  }
);

const Socio = mongoose.model('Socio', socioSchema);
module.exports = Socio;