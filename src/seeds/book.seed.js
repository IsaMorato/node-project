const mongoose = require("mongoose")
const Book = require ('../models/book.models')

require('dotenv').config()



const books =[
    {
        title: "No soy un monstruo",
        writer: "Carme Chaparro",
        year:2017
    },
    {
        title: "El alma de las flores",
        writer: "Vivina Rivero",
        year:2019
    },

    {
        title: "Sira",
        writer: "María Dueñas",
        year:2021
    }


];

mongoose
.connect(process.env.MONGODBURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
        // Utilizando Book.find() obtendremos un array con todos los personajes de la db
    const allBooks = await Book.find();
        // Si existen personajes previamente, dropearemos la colección
    if (allBooks.length) {
      await Book.collection.drop(); //La función drop borra la colección
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
        // Una vez vaciada la db de los personajes, usaremos el array characterDocuments
        // para llenar nuestra base de datos con todas los personajes.
        await Book.insertMany(books);
    })
  .catch((err) => console.log(`Error creating data: ${err}`))
    // Por último nos desconectaremos de la DB.
  .finally(() => mongoose.disconnect());